from django.shortcuts import render
from django.http import HttpResponse
from .models import WebsiteModel
from django.template import loader


# nginx port: 8080
def index(request):
    all_objects = WebsiteModel.objects.all()
    lat = WebsiteModel.latitude
    lng = WebsiteModel.longitude
    template = loader.get_template('website/index.html')
    context = {'context': all_objects,
               'latitude': lat,
               'longitude': lng}
    return HttpResponse(template.render(context, request))


# ports 7001, 7002, 7003
def last_tracking_by_device(request):
    all_objects = WebsiteModel.objects.all()
    latitude = WebsiteModel.objects.latest('latitude')
    longitude = WebsiteModel.objects.latest('longitude')
    context = {'all_data': all_objects,
               'long': longitude,
               'lat': latitude}
    return render(request, 'website/index.html', context)


# ports: 8001, 8002, 8003
def add_tracking(request):
    new_device = WebsiteModel()
    latitude = request.GET.get('latitude')
    longitude = request.GET.get('longitude')
    new_device.latitude = latitude
    new_device.longitude = longitude
    new_device.save()
    return HttpResponse("Added Latitude: " + str(latitude) + "\t" + "Added Longitude: " + str(longitude) + "\n")
