Web tracking app.
Goal: collect data from mobile device (Kotlin in our case) and display geolocation on the website.
Mobile device shares its geolocation (longitude, latitude) and then using NGINX load-balancer the high-load will be spread between servers.
In our case, we have 3 pre-defined servers on Microsoft Azure. 

Firsly, we need to divide the app into 2 pieces: one of it will read the app's result, the second will send geolocation from the mobile device.
After that, NGINX technology will balance the load to diminish the degree of load to the server, and then put nginx.conf (NGINX configuration file) 
inside the Dockerfile.
Before deploying it on the servers, I containerized our project using Docker technology, by writing **Dockerfile** and **docker-compose.yml** 
