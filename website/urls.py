from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('last-tracking-by-device', views.last_tracking_by_device, name="last_tracking_by_device"),
    path('add-tracking', views.add_tracking, name="add-tracking"),
]