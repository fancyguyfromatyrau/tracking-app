import os
from django.core.wsgi import get_wsgi_application
import website


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tracking.settings')
application = get_wsgi_application()
